from django.core.management.base import BaseCommand, CommandError

from schedule.export import generate_xlsx


class Command(BaseCommand):

    def handle(self, *args, **options):
        generate_xlsx(**options)

        self.stdout.write(self.style.SUCCESS('Successfully exported'))
