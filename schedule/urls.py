from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from schedule.views import EventViewSet, GenerateViewSet, DownloadXlsxViewSet, SchedulesViewSet

router = routers.DefaultRouter()
router.register(r"generate", GenerateViewSet, basename="generate")
router.register(r"events", EventViewSet, basename="event")
router.register(r"xlsx", DownloadXlsxViewSet, basename="xlsx")
router.register("", SchedulesViewSet, basename="schedules")

urlpatterns = [url(r"^", include(router.urls), name="schedules")]
