from rest_framework import serializers
from .models import *


class TeacherPreferenceSerializer(serializers.ModelSerializer):
    teacher_id = serializers.PrimaryKeyRelatedField(source='teacher', read_only=True)
    timeslot_id = serializers.PrimaryKeyRelatedField(source='timeslot', read_only=False, queryset=Timeslot.objects.all())
    priority = serializers.IntegerField(read_only=False)

    class Meta:
        model = TeacherPreference
        fields = ('id', 'teacher_id', 'timeslot_id', 'priority')
