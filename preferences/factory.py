import factory
from factory import fuzzy

from .models import *
from schedule.models import Timeslot


class TeacherPreferenceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TeacherPreference

    teacher = fuzzy.FuzzyChoice(User.teacher_queryset())
    timeslot = fuzzy.FuzzyChoice(Timeslot.objects.all())
    priority = fuzzy.FuzzyInteger(1, 10)
