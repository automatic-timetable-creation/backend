from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from schedule.models import Timeslot
from users.models import User


class TeacherPreference(models.Model):
    class Meta:
        unique_together = ('teacher', 'timeslot')

    teacher = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to=User.teacher_queryset)
    timeslot = models.ForeignKey(Timeslot, on_delete=models.CASCADE)
    priority = models.SmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)], null=True)
