from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .serializers import *
from .models import *
from users.permissions import IsTeacher


class TeacherPreferenceViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsTeacher)
    queryset = TeacherPreference.objects.all()
    serializer_class = TeacherPreferenceSerializer

    def get_queryset(self):
        return TeacherPreference.objects.filter(teacher=self.request.user.id)
